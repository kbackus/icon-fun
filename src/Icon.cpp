#include "Icon.h"

Icon::Icon(int iconId, POINT point, RECT rect)
{
    this->iconId = iconId;
    this->dirty = true;

    this->x = (int)((rect.right + rect.left) * 0.5);
    this->y = (int)((rect.top + rect.bottom) * 0.5);

    this->offset.x = (int)(point.x - this->x);
    this->offset.y = (int)(point.y - this->y);

    this->start.x = this->x;
    this->start.y = this->y;

    this->rect = rect;
}

void Icon::setIconContext(HWND desktopListView, HANDLE process, LPVOID virtualPoint) {
    this->desktopListView = desktopListView;
    this->process = process;
    this->virtualPoint = virtualPoint;
}

float Icon::distanceFromMouse(POINT mouse) {
    return this->fastsqrt((this->x - mouse.x)*(this->x - mouse.x)
        +(this->y - mouse.y)*(this->y - mouse.y));
}

float Icon::distanceFromStart() {
    return this->fastsqrt((this->x - this->start.x)*(this->x - this->start.x)
        +(this->y - this->start.y)*(this->y - this->start.y));
}

void Icon::pushAwayFromPoint(POINT mouse, float radius, float dist) {
    if (dist > radius)
        return;

    this->dirty = true;
    this->x = mouse.x + radius * (this->x - mouse.x) / dist;
    this->y = mouse.y + radius * (this->y - mouse.y) / dist;
}

void Icon::moveTowardStart() {
    float newX = this->start.x + (this->x - this->start.x) * 0.5;
    float newY = this->start.y + (this->y - this->start.y) * 0.5;
    dirty = dirty || (newX != this->x || newY != this->y);
    this->x = newX;
    this->y = newY;
}

void Icon::moveIcon() {
    if (!dirty)
        return;

    dirty = false;

    POINT newPosition;
    newPosition.x = (int)(this->x + this->offset.x);
    newPosition.y = (int)(this->y + this->offset.y);

    if (newPosition.x == this->lastPosition.x && newPosition.y == this->lastPosition.y)
        return;

    this->lastPosition = newPosition;

    WriteProcessMemory(this->process, this->virtualPoint, (LPVOID)&(newPosition), sizeof(POINT), NULL);
    SendMessage(desktopListView, LVM_SETITEMPOSITION32, (WPARAM)this->iconId, (LPARAM)this->virtualPoint);
}

Icon::~Icon()
{
}
