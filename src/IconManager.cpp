#include "IconManager.h"

IconManager::IconManager(HANDLE process, HWND handleListView, POINT* virtualPoint, RECT* virtualRect, int mouseRadius)
{
    this->handleListView = handleListView;
    this->mouseRadius = mouseRadius;
    this->process = process;
    this->virtualPoint = virtualPoint;

    RECT iconRect;
    POINT absIconPoint;
    int icons = (int)SendMessage(handleListView, LVM_GETITEMCOUNT, NULL, NULL);
    for (int i=0; i<icons; i++) {
        iconRect.left = LVIR_BOUNDS;
        WriteProcessMemory(process, virtualRect, &iconRect, sizeof(RECT), NULL);
        SendMessage(handleListView, LVM_GETITEMRECT, (WPARAM)i, (LPARAM)virtualRect);
        SendMessage(handleListView, LVM_GETITEMPOSITION, (WPARAM)i, (LPARAM)virtualPoint);
        ReadProcessMemory(process, virtualRect, &iconRect, sizeof(RECT), NULL);
        ReadProcessMemory(process, virtualPoint, &absIconPoint, sizeof(POINT), NULL);

        this->addIcon(Icon(i, absIconPoint, iconRect));
    }
}

void IconManager::play() {
    for(std::vector<Icon>::iterator itr = icons.begin(); itr != icons.end(); ++itr) {
        itr->moveTowardStart();
        float dist = itr->distanceFromMouse(this->lastMousePos);
        if (dist < this->mouseRadius && dist > 0)
            itr->pushAwayFromPoint(this->lastMousePos, this->mouseRadius, dist);
        itr->moveIcon();
    }
}

void IconManager::addIcon(Icon icon) {
    icon.setIconContext(this->handleListView, this->process, this->virtualPoint);
    this->icons.push_back(icon);
}

void IconManager::setMousePos(POINT point) {
    if (this->lastMousePos.x == point.x && this->lastMousePos.y == point.y)
        return;

    lastMousePos.x = point.x;
    lastMousePos.y = point.y;
}

IconManager::~IconManager()
{

}
