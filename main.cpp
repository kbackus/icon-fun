#include <iostream>

#define _WIN32_WINNT 0x0600
#include <windows.h>
#include <commctrl.h>
#include <winuser.h>
#include <tchar.h>
#include <math.h>
#include <IconManager.h>

HHOOK hMouseHook;
HWND hDesktopListView;
POINT *_pt;
RECT *_rect;
HANDLE process;
IconManager* iconManager;

bool terminate = false;

HWND GetDesktopListViewHWND()
{
    HWND hDesktopListView = NULL;
    HWND hWorkerW = NULL;
    HWND hProgman = FindWindow(_T("Progman"), 0);
    HWND hDesktopWnd = GetDesktopWindow();

    if (!hProgman)
        return NULL;

    HWND hShellViewWin = FindWindowEx(hProgman, 0, _T("SHELLDLL_DefView"), 0);
    if (hShellViewWin) {
        hDesktopListView = FindWindowEx(hShellViewWin, 0, _T("SysListView32"), 0);
    } else {
        do {
            hWorkerW = FindWindowEx( hDesktopWnd, hWorkerW, _T("WorkerW"), NULL );
            hShellViewWin = FindWindowEx(hWorkerW, 0, _T("SHELLDLL_DefView"), 0);
        } while (hShellViewWin == NULL && hWorkerW != NULL);
    }

    hDesktopListView = FindWindowEx(hShellViewWin, 0, _T("SysListView32"), 0);

    return hDesktopListView;
}

LRESULT CALLBACK OnHandleMouseMove (int nCode, WPARAM wParam, LPARAM lParam)
{
    MOUSEHOOKSTRUCT *pMouseStruct = (MOUSEHOOKSTRUCT*)lParam;
    int icons = (int)SendMessage(hDesktopListView, LVM_GETITEMCOUNT, NULL, NULL);

    if (pMouseStruct == NULL)
        return CallNextHookEx(hMouseHook, nCode, wParam, lParam);

    POINT mousePoint = pMouseStruct->pt;
    iconManager->setMousePos(mousePoint);
    return CallNextHookEx(hMouseHook, nCode, wParam, lParam);
}

DWORD WINAPI WatchMouse(LPVOID lpParam) {
    HINSTANCE instance = GetModuleHandle(NULL);
    hMouseHook = SetWindowsHookEx(WH_MOUSE_LL, OnHandleMouseMove, instance, NULL);
    MSG message;
    while(GetMessage(&message, NULL, 0, 0)) {
        TranslateMessage(&message);
        DispatchMessage(&message);
    }

    UnhookWindowsHookEx(hMouseHook);
    return 0;
}

DWORD WINAPI MoveAllIcons(LPVOID lpParam) {
    while(!terminate) {
        iconManager->play();
        Sleep(24);
    }
}

int main(int argc, char** argv)
{
    hDesktopListView = GetDesktopListViewHWND();

    DWORD dwStyle = GetWindowLong(hDesktopListView, GWL_STYLE);
    SetWindowLong(hDesktopListView, GWL_STYLE, dwStyle & ~LVS_AUTOARRANGE & ~LVS_ALIGNMASK);

    unsigned long processId;
    GetWindowThreadProcessId(hDesktopListView, &processId);

    DWORD procFlags = PROCESS_VM_OPERATION|PROCESS_VM_READ|PROCESS_VM_WRITE|PROCESS_QUERY_INFORMATION;
    process = OpenProcess(procFlags, FALSE, processId);

    _rect = (RECT*)VirtualAllocEx(process, NULL, sizeof(RECT), MEM_COMMIT, PAGE_READWRITE);
    _pt = (POINT*)VirtualAllocEx(process, NULL, sizeof(POINT), MEM_COMMIT, PAGE_READWRITE);

    iconManager = new IconManager(process, hDesktopListView, _pt, _rect, 128);

    POINT point;
    DWORD dwThread;
    HANDLE mouseThread = CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)WatchMouse, (LPVOID)argv[0], NULL, &dwThread);
    HANDLE handleThread = CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)MoveAllIcons, (LPVOID)argv[0], NULL, &dwThread);

    if (!mouseThread)
        return 1;

    int retValue = WaitForSingleObject(mouseThread, INFINITE);
    terminate = true;
    WaitForSingleObject(handleThread, INFINITE);

    VirtualFreeEx(process, _pt, 0, MEM_RELEASE);
    delete iconManager;

    return retValue;
}
