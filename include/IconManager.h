#ifndef ICONMANAGER_H
#define ICONMANAGER_H

#include <vector>
#include <windows.h>
#include <Icon.h>

class IconManager
{
    public:
        IconManager(HANDLE process, HWND handleListView, POINT* virtualPoint, RECT* virtualRect, int mouseRadius);
        virtual ~IconManager();

        void play();
        void setMousePos(POINT point);
        void addIcon(Icon icon);

    protected:

    private:
        HANDLE process;
        HWND handleListView;
        POINT* virtualPoint;

        std::vector<Icon> icons;
        int mouseRadius;
        POINT lastMousePos;
};

#endif // ICONMANAGER_H
