#ifndef ICON_H
#define ICON_H

#include <iostream>
#include <windows.h>
#include <commctrl.h>

class Icon
{
    public:
        Icon(int iconId, POINT point, RECT rect);
        virtual ~Icon();

        void setIconContext(HWND desktopListView, HANDLE process, LPVOID virtualPoint);

        float distanceFromMouse(POINT mouse);
        void pushAwayFromPoint(POINT mouse, float radius, float dist);
        void moveTowardStart();
        void moveIcon();

    protected:

    private:
        HWND desktopListView;
        HANDLE process;
        LPVOID virtualPoint;

        int iconId;
        bool dirty;
        POINT offset, start, lastPosition;
        RECT rect;

        float x, y;

        float distanceFromStart();

        float fastsqrt(float val) {
            int tmp = *(int *)&val;
            tmp -= 1<<23;
            tmp = tmp >> 1;
            tmp += 1<<29;
            return *(float *)&tmp;
        }
};

#endif // ICON_H
